FROM registry.gitlab.com/xangelix-pub/arch/arch-novnc:latest
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"
EXPOSE 8080

RUN yay -Syu --noconfirm librewolf-bin
