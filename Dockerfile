FROM docker.io/archlinux:base-devel
LABEL maintainer="Cody Wyatt Neiman (xangelix) <neiman@cody.to>"
EXPOSE 8080

RUN pacman -Syu --noconfirm curl inetutils tigervnc supervisor git
RUN pacman -Syu --noconfirm openbox ttf-font xterm

RUN curl -sSL https://github.com/tianon/gosu/releases/download/1.14/gosu-amd64 \
    > /usr/bin/gosu && chmod +x /usr/bin/gosu

RUN useradd --no-log-init --uid 33333 --user-group --groups wheel --create-home \
    --home-dir /data --shell /bin/bash --password app app
RUN sed -i \
    's!# %wheel ALL=(ALL:ALL) NOPASSWD: ALL!%wheel ALL=(ALL:ALL) NOPASSWD: ALL!g' \
    /etc/sudoers

ENV DISPLAY=:0

USER app
ENV pkgz=yay
RUN cd /data && git clone https://aur.archlinux.org/${pkgz}.git && cd ${pkgz} \
    && makepkg -si --noconfirm

RUN yay -S --noconfirm easy-novnc-git

USER root
COPY supervisord.conf /etc/

CMD ["sh", "-c", "chown app:app /data /dev/stdout && exec gosu app supervisord"]
